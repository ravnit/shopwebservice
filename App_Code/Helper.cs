﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace RavnitShopSaveOrderToXml.logic
{
    public class Helper
    {
        public enum SaveType { OrderConfirmationBackup, OrderProcessedBackup, Exception }

        public static string GetEnvironment() 
        {
            string env = ConfigurationManager.AppSettings["Environment"];

            switch (env.ToLower())
            {
                case "test":
                    return "Test";
                    break;
                case "live":
                    return "Live";
                    break;
                default:
                    return "Test";
                    break;
            }
        
        }
        public static string GetValueFromNodeList(XmlNodeList nodelist, string nodeName)
        {
            string retval = "";
            XmlNode node = nodelist[0].SelectSingleNode(nodeName);


            if (node != null)
            {
                retval = node.InnerText;
            }

            return retval;
    
        }

        public static string GetValueFromNode(XmlNode node, string nodeName)
        {
            string retval = "";
            if (node != null)
            {
                var selectSingleNode = node.SelectSingleNode(nodeName);
                if (selectSingleNode != null) retval = selectSingleNode.InnerText;
            }

            return retval;
        }

        public static bool SaveXML(SaveType type, string xml, string ordreNr, string error = "")
        {

            bool retval = false;
           
            string filepath = GetFilePath(type, ordreNr);

            if(error != "")
            {
                xml = error + "\n\n\n\n\n\n\n" + xml; 
            }
            

            System.IO.File.WriteAllText(filepath, xml);
            return retval;
        }

        public static void ProcessErrors(string xml, Exception ex) 
        {
            try
            {
                SaveXML(SaveType.Exception, xml, ex.ToString());

                var fromAddress = new MailAddress("ravnittest@gmail.com", "Fejl i webservice");
                var toAddress = new MailAddress("morten@bork.dk", "Morten Bork");
                const string fromPassword = "login2web";
                const string subject = "Fejl ved behandling af order";
                string body = ex.ToString() + "\n\n\n\n\n\n\n" + xml;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                } 


            }
            catch (Exception)
            {
              
            }
            

        }

        private static string GetFilePath(SaveType type, string uniquenr)
        {
            string retval = "";
            switch (type)
            {
                case SaveType.OrderConfirmationBackup:
                    retval = ConfigurationManager.AppSettings["OrderConfirmationBackupPath"];
                    break;
                case SaveType.OrderProcessedBackup:
                    retval = ConfigurationManager.AppSettings["OrderProcessedBackupPath"];
                    break;
                case SaveType.Exception:
                    retval = ConfigurationManager.AppSettings["ExceptionPath"];
                    break;

            }

            retval = string.Format(retval, uniquenr + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + DateTime.Now.Millisecond);

            return retval;
        }


    }
}