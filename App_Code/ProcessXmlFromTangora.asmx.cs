﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Data;
using System.Xml;
using RavnitShopSaveOrderToXml.logic;

namespace RavnitShopSaveOrderToXml
{
    /// <summary>
    /// Summary description for ProcessXmlFromTangora
    /// </summary>
    [WebService(Namespace = "http://ravnit")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProcessXmlFromTangora : System.Web.Services.WebService
    {
      
            
        [WebMethod]

        public int OrderProcessed(string xml)
        {
            try
            {
            

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(xml);
                XmlNodeList customerinfoNodeList = xd.SelectNodes("/order/customerinfo");
            

                string ordreNr = Helper.GetValueFromNodeList(customerinfoNodeList, "Faktnr");

XmlElement root = xd.DocumentElement;
root.RemoveAll();

XmlElement cust = xd.CreateElement("customerinfo");
 root.AppendChild(cust);

foreach (XmlNode xn in customerinfoNodeList) {
    XmlNode totalsum = xn["Totalsum"]; 
    XmlNode deltagernr = xn["Deltagernr"]; 
    cust.AppendChild(deltagernr);
    cust.AppendChild(totalsum);
}


StringWriter sw = new StringWriter();
	XmlTextWriter xw = new XmlTextWriter(sw);
	xd.WriteTo(xw);

              //  Helper.SaveXML(Helper.SaveType.OrderProcessedBackup, xml, ordreNr);
                Helper.SaveXML(Helper.SaveType.OrderProcessedBackup, sw.ToString(), ordreNr);

                //CreateC5Xml.Run(xml, ordreNr);
            }
            catch (Exception ex)
            {
                Helper.ProcessErrors(xml, ex);
                throw ex;
            }
            
            
         
            return 1;
        }
       
    }
}
